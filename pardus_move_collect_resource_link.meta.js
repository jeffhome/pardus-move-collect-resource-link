// ==UserScript==
// @name        Pardus Move Collect Resource Link
// @namespace   http://userscripts.xcom-alliance.info/
// @description Moves the Collect Resource link to the bottom of the Commands box of the main Navigation screen.
// @author      Miche (Orion) / Sparkle (Artemis)
// @version     1.1
// @include     http*://*.pardus.at/main.php*
// @updateURL 	http://userscripts.xcom-alliance.info/move_collect_resource_link/pardus_move_collect_resource_link.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/move_collect_resource_link/pardus_move_collect_resource_link.user.js
// @icon 		http://userscripts.xcom-alliance.info/move_collect_resource_link/icon.png
// @grant       none
// ==/UserScript==
