// ==UserScript==
// @name        Pardus Move Collect Resource Link
// @namespace   http://userscripts.xcom-alliance.info/
// @description Moves the Collect Resource link to the bottom of the Commands box of the main Navigation screen.
// @author      Miche (Orion) / Sparkle (Artemis)
// @version     1.1
// @include     http*://*.pardus.at/main.php*
// @updateURL 	http://userscripts.xcom-alliance.info/move_collect_resource_link/pardus_move_collect_resource_link.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/move_collect_resource_link/pardus_move_collect_resource_link.user.js
// @icon 		http://userscripts.xcom-alliance.info/move_collect_resource_link/icon.png
// @grant       none
// ==/UserScript==

var MoveCollectResourceLink = {
	elArr: [],
	Init: function() {
		if (document.getElementById('commands_content')) this.Update();
	},
	Update: function() {
		if (document.getElementById('commands_content')) {
			this.GetCollectResourceLink();
			this.MoveLink();
		}
	},
	GetCollectResourceLink: function() {
		var startEl = document.getElementById('aCmdCollect').previousSibling;
		var endEl = document.getElementById('aCmdCollect5').nextSibling;
		if (startEl && endEl) {
			var tempEl = startEl;
			while (tempEl.nextSibling && tempEl !== endEl) {
				nextEl = tempEl.nextSibling;
				this.elArr.push(tempEl.parentNode.removeChild(tempEl));
				tempEl = nextEl;
			}
		}
	},
	MoveLink: function() {
		while (this.elArr.length) document.getElementById('commands_content').appendChild(this.elArr.shift());
	}
};
	
MoveCollectResourceLink.Init();

unsafeWindow.addUserFunction(function(MCRL) {
	return function() {
		MCRL.Update();
	};
}(MoveCollectResourceLink));